import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private url:string = "https://localhost:44304/api/";
  
  constructor(private http: Http) { }

  protected getJsonArray(path: string): Observable<any> {
    return this.http.get(this.url+path).pipe(map(response => {
      return response.json();
    }));
  }

  protected getJsonObj(path: string, id: number): Observable<any> {
    return this.http.get(this.url+path+id).pipe(map(response => {
      return response.json();
    }));
  }

  protected createJsonObj<T>(path: string, body: T): Observable<any> {
    return this.http.post(this.url+path, body);
  }

  protected updateJsonObj<T>(path: string, id: number, body: T): Observable<any> {
    return this.http.put(this.url+path+id, body);
  }
}
