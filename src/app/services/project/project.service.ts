import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Project } from 'src/app/models/project/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService extends ApiService {
  private path: string = "project/"
  constructor(http: Http) { 
    super(http);
  }

  getProject(id: number): Observable<Project>{
    return this.getJsonObj(this.path,id);
  }

  getProjects(): Observable<Project[]>{
    return this.getJsonArray(this.path);
  }

  updateProjects(id:number, obj: Project): Observable<any>{
    return this.updateJsonObj(this.path, id, obj);
  }

  createProjects(obj: Project): Observable<any>{
    return this.createJsonObj(this.path, obj);
  }
}
