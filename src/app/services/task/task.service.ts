import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Task } from 'src/app/models/task/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService extends ApiService {
  private path: string = "task/"
  constructor(http: Http) { 
    super(http);
  }

  getTask(id: number): Observable<Task>{
    return this.getJsonObj(this.path,id);
  }

  getTasks(): Observable<Task[]>{
    return this.getJsonArray(this.path);
  }

  updateTask(id:number, obj: Task): Observable<void>{
    return this.updateJsonObj(this.path, id, obj);
  }

  createTask(obj: Task): Observable<void>{
    return this.createJsonObj(this.path, obj);
  }
}
