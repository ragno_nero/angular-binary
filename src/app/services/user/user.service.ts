import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user/user';

@Injectable({
  providedIn: 'root'
})
export class UserService extends ApiService  {
  private path: string = "user/"
  constructor(http: Http) { 
    super(http);
  }

  getUser(id: number): Observable<User>{
    return this.getJsonObj(this.path,id);
  }

  getUsers(): Observable<User[]>{
    return this.getJsonArray(this.path);
  }

  updateUser(id:number, obj: User): Observable<void>{
    return this.updateJsonObj(this.path, id, obj);
  }

  createUser(obj: User): Observable<void>{
    return this.createJsonObj(this.path, obj);
  }
}
