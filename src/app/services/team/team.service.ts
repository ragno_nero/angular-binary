import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { Team } from 'src/app/models/team/team';

@Injectable({
  providedIn: 'root'
})
export class TeamService extends ApiService {
  private path: string = "team/"
  constructor(http: Http) { 
    super(http);
  }

  getTeam(id: number): Observable<Team>{
    return this.getJsonObj(this.path,id);
  }

  getTeams(): Observable<Team[]>{
    return this.getJsonArray(this.path);
  }

  updateTeam(id:number, obj: Team): Observable<void>{
    return this.updateJsonObj(this.path, id, obj);
  }

  createTeam(obj: Team): Observable<void>{
    return this.createJsonObj(this.path, obj);
  }
}
