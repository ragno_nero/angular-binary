import { Directive, Input, ElementRef, HostListener, SimpleChanges, OnChanges } from '@angular/core';

@Directive({
  selector: '[appStateColor]'
})
export class StateColorDirective implements OnChanges {

  @Input('appStateColor') state: number = 0;

  constructor(private elem: ElementRef) {
    this.changeBackground("grey");
  }

  ngOnChanges(changes: SimpleChanges){
    switch (this.state){
      case 0:
        this.changeBackground("grey");
        break;
      case 1:
        this.changeBackground("blue");
        break;
      case 2:
        this.changeBackground("yellow");
        break;
      case 3:
        this.changeBackground("green");
        break;
      case 4:
        this.changeBackground("red");
        break;
   }
  }

  changeBackground(color: string){
    this.elem.nativeElement.style.backgroundColor = color;
  }
}
