export class Project {
    id: number;
    name: string;
    description: string;
    createdAt: string;
    deadline: string;
    authorId: number;
    teamId: number;
}
