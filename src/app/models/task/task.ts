export class Task {
    id: number;
    name: string;
    description: string;
    createdAt: string;
    finishedAt: string;
    state: number;
    projectId: number;
    performerId: number;
}
