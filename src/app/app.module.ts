import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HomeComponent } from './components/home/home.component';
import { UserComponent } from './components/user/user.component';
import { ProjectComponent } from './components/project/project.component';
import { TaskComponent } from './components/task/task.component';
import { TeamComponent } from './components/team/team.component';
import { UADatePipe } from './pipes/ua-date.pipe';
import { UnsavedChangesGuard } from './guard/unsaved-changes.guard';
import { HttpModule } from '@angular/http';
import { ViewProjectComponent } from './components/project/children/view-project/view-project.component';
import { CreateProjectComponent } from './components/project/children/create-project/create-project.component';
import { CreateTaskComponent } from './components/task/children/create-task/create-task.component';
import { ViewTaskComponent } from './components/task/children/view-task/view-task.component';
import { ViewTeamComponent } from './components/team/children/view-team/view-team.component';
import { CreateTeamComponent } from './components/team/children/create-team/create-team.component';
import { ViewUserComponent } from './components/user/children/view-user/view-user.component';
import { CreateUserComponent } from './components/user/children/create-user/create-user.component';
import { EditProjectComponent } from './components/project/children/edit-project/edit-project.component';
import { EditTaskComponent } from './components/task/children/edit-task/edit-task.component';
import { EditTeamComponent } from './components/team/children/edit-team/edit-team.component';
import { EditUserComponent } from './components/user/children/edit-user/edit-user.component';
import { FormsModule, ReactiveFormsModule }         from '@angular/forms';
import { StateColorDirective } from './directive/state-color.directive';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HomeComponent,
    UADatePipe,
    UserComponent,
    ProjectComponent,
    TaskComponent,
    TeamComponent,
    UADatePipe,
    ViewProjectComponent,
    CreateProjectComponent,
    CreateTaskComponent,
    ViewTaskComponent,
    ViewTeamComponent,
    CreateTeamComponent,
    ViewUserComponent,
    CreateUserComponent,
    EditProjectComponent,
    EditTaskComponent,
    EditTeamComponent,
    EditUserComponent,
    StateColorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    ReactiveFormsModule
  ],
  providers: [UnsavedChangesGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
