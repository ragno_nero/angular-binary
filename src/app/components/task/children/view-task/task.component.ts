import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task/task';
import { TaskService } from 'src/app/services/task/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
  providers: [TaskService]
})
export class TaskComponent implements OnInit {

  private taskArr: Task[] = []; 
  private componentRef: any;
  constructor(private _taskService: TaskService) { }

  ngOnInit() {
    this._taskService.getTasks().subscribe(task=> 
      {
        this.taskArr = task;
        this.componentRef.TransferToChild(this.taskArr)
      });
  }
  onActivate(componentReference) {
    this.componentRef = componentReference;
 }
}
