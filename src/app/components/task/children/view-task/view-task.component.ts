import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task/task';
import { TaskService } from 'src/app/services/task/task.service';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.scss'],
  providers: [TaskService]
})
export class ViewTaskComponent implements OnInit {
  private taskArr: Task[];
  constructor(private _taskService: TaskService) { }

  ngOnInit() {
    this._taskService.getTasks().subscribe(task=> 
      {
        this.taskArr = task;
      });
  }
}
