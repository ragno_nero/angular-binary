import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/interfaces/component-can-deactivate';
import { Observable } from 'rxjs';
import { UnsavedChangesGuard } from 'src/app/guard/unsaved-changes.guard';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Task } from 'src/app/models/task/task';
import { Router } from '@angular/router';
import { TaskService } from 'src/app/services/task/task.service';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit, UnsavedChangesGuard {
  _taskForm: FormGroup;
  private _task: Task = new Task();
  private _saved = false;
  canDeactivate(component: ComponentCanDeactivate): boolean | Observable<boolean> | Promise<boolean> {
    if(!this._saved){
      return confirm("Do you want to leave the page?");
    }
    else{
      return true;
    }
  }
  
  constructor(private _taskService: TaskService, private _router: Router) { 
    this._taskForm = new FormGroup({       
      "taskName": new FormControl("", [Validators.required, Validators.minLength(4)]),
      "taskDescription": new FormControl("", Validators.required),
      "taskCreatedAt": new FormControl("", Validators.required),
      "taskFinishedAt": new FormControl("", Validators.required),
      "taskState": new FormControl("", Validators.required),
      "taskProjectId": new FormControl("", Validators.required),
      "taskPerformerId": new FormControl("", Validators.required)
  });
  }

  ngOnInit() {
 }

  save() {
    this._saved = true;
    this._task.name= this._taskForm.value['taskName'];
    this._task.description= this._taskForm.value['taskDescription'];
    this._task.createdAt= this._taskForm.value['taskCreatedAt'];
    this._task.finishedAt= this._taskForm.value['taskFinishedAt'];
    this._task.state = this._taskForm.value['taskState'];
    this._task.projectId = this._taskForm.value['taskProjectId'];
    this._task.performerId = this._taskForm.value['taskPerformerId'];
    this._taskService.createTask(this._task);
    this._router.navigate(['/task']);
  }

}
