import { Component, OnInit} from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/interfaces/component-can-deactivate';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Task } from 'src/app/models/task/task';
import { TaskService } from 'src/app/services/task/task.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.scss']
})
export class EditTaskComponent implements OnInit {
  private _id: number;
  _taskForm: FormGroup;
  private _task: Task;
  private _saved = false;
  canDeactivate(component: ComponentCanDeactivate): boolean | Observable<boolean> | Promise<boolean> {
    if(!this._saved){
      return confirm("Do you want to leave the page?");
    }
    else{
      return true;
    }
  }
  
  constructor(private _taskService: TaskService, private _router: Router, private _activatedRoute: ActivatedRoute) { 
    this._taskForm = new FormGroup({       
      "taskName": new FormControl("", [Validators.required, Validators.minLength(4)]),
      "taskDescription": new FormControl("", Validators.required),
      "taskCreatedAt": new FormControl("", Validators.required),
      "taskFinishedAt": new FormControl("", Validators.required),
      "taskState": new FormControl("", Validators.required),
      "taskProjectId": new FormControl("", Validators.required),
      "taskPerformerId": new FormControl("", Validators.required)
  });
  }

  ngOnInit() {
    this._id = this._activatedRoute.snapshot.params.id;
    this._taskService.getTask(this._id).subscribe(task=> {
        this._task = task;
        this._taskForm.controls['taskName'].setValue(this._task.name);
        this._taskForm.controls['taskDescription'].setValue(this._task.description);
        this._taskForm.controls['taskCreatedAt'].setValue(this._task.createdAt);
        this._taskForm.controls['taskFinishedAt'].setValue(this._task.finishedAt);
        this._taskForm.controls['taskState'].setValue(this._task.state);
        this._taskForm.controls['taskProjectId'].setValue(this._task.projectId);
        this._taskForm.controls['taskPerformerId'].setValue(this._task.performerId);
      });   
  }

  save() {
    this._saved = true;
    this._task.name= this._taskForm.value['taskName'];
    this._task.description= this._taskForm.value['taskDescription'];
    this._task.createdAt= this._taskForm.value['taskCreatedAt'];
    this._task.finishedAt= this._taskForm.value['taskFinishedAt'];
    this._task.state = this._taskForm.value['taskState'];
    this._task.projectId = this._taskForm.value['taskProjectId'];
    this._task.performerId = this._taskForm.value['taskPerformerId'];
    this._taskService.updateTask(this._id, this._task);
    this._router.navigate(['/task']);
  }

}
