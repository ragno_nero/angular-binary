import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/interfaces/component-can-deactivate';
import { Observable } from 'rxjs';
import { TeamService } from 'src/app/services/team/team.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Team } from 'src/app/models/team/team';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.scss'],
  providers: [TeamService]
})
export class EditTeamComponent implements OnInit {
  private _id: number;
  _teamForm: FormGroup;
  private _team: Team;
  private _saved = false;
  canDeactivate(component: ComponentCanDeactivate): boolean | Observable<boolean> | Promise<boolean> {
    if(!this._saved){
      return confirm("Do you want to leave the page?");
    }
    else{
      return true;
    }
  }
  
  constructor(private _teamService: TeamService, private _router: Router, private _activatedRoute: ActivatedRoute) { 
    this._teamForm = new FormGroup({       
      "teamName": new FormControl("", [Validators.required, Validators.minLength(3)]),
      "teamCreatedAt": new FormControl("", Validators.required)
  });
  }

  ngOnInit() {
    this._id = this._activatedRoute.snapshot.params.id;
    this._teamService.getTeam(this._id).subscribe(team=> {
        this._team = team;
        this._teamForm.controls['teamName'].setValue(this._team.name);
        this._teamForm.controls['teamCreatedAt'].setValue(this._team.createdAt);
      });   
  }

  save() {
    this._saved = true;
    this._team.name= this._teamForm.value['teamName'];
    this._team.createdAt= this._teamForm.value['teamCreatedAt'];
    this._teamService.updateTeam(this._id, this._team);
    this._router.navigate(['/team']);
  }
}
