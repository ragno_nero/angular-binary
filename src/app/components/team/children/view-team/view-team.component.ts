import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/services/team/team.service';
import { Team } from 'src/app/models/team/team';

@Component({
  selector: 'app-view-team',
  templateUrl: './view-team.component.html',
  styleUrls: ['./view-team.component.scss'],
  providers: [TeamService]
})
export class ViewTeamComponent implements OnInit {
  private teamArr: Team[] = [];
  constructor(private _teamService: TeamService) { }

  ngOnInit() {
    this._teamService.getTeams().subscribe(team=> 
      {
        this.teamArr = team;
      });
  }
}
