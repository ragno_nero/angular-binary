import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/interfaces/component-can-deactivate';
import { Observable } from 'rxjs';
import { UnsavedChangesGuard } from 'src/app/guard/unsaved-changes.guard';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Team } from 'src/app/models/team/team';
import { TeamService } from 'src/app/services/team/team.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss']
})
export class CreateTeamComponent implements OnInit, UnsavedChangesGuard {
  _teamForm: FormGroup;
  private _team: Team = new Team();
  private _saved = false;
  canDeactivate(component: ComponentCanDeactivate): boolean | Observable<boolean> | Promise<boolean> {
    if(!this._saved){
      return confirm("Do you want to leave the page?");
    }
    else{
      return true;
    }
  }
  
  constructor(private _teamService: TeamService, private _router: Router) { 
    this._teamForm = new FormGroup({       
      "teamName": new FormControl("", [Validators.required, Validators.minLength(3)]),
      "teamCreatedAt": new FormControl("", Validators.required)
  });
  }

  ngOnInit() {
  }

  save() {
    this._saved = true;
    this._team.name = this._teamForm.value['teamName'];
    this._team.createdAt= this._teamForm.value['teamCreatedAt'];
    this._teamService.createTeam(this._team);
    this._router.navigate(['/team']);
  }
}
