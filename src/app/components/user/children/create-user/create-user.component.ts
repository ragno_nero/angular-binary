import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/interfaces/component-can-deactivate';
import { UnsavedChangesGuard } from 'src/app/guard/unsaved-changes.guard';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { User } from 'src/app/models/user/user';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit, UnsavedChangesGuard {
  _userForm: FormGroup;
  private _user: User = new User();
  private _saved = false;
  canDeactivate(component: ComponentCanDeactivate): boolean | Observable<boolean> | Promise<boolean> {
    if(!this._saved){
      return confirm("Do you want to leave the page?");
    }
    else{
      return true;
    }
  }
  
  constructor(private _userService: UserService, private _router: Router) { 
    this._userForm = new FormGroup({       
      "userFirstname": new FormControl("", [Validators.required, Validators.minLength(4)]),
      "userLastname": new FormControl("", [Validators.required, Validators.minLength(4)]),
      "userEmail": new FormControl("", [Validators.required, Validators.email]),
      "userBirthday": new FormControl("", Validators.required),
      "userRegisteredAt": new FormControl("", Validators.required),
      "userTeamId": new FormControl("", Validators.required)
  });
  }

  ngOnInit() {
  }

  save() {
    this._saved = true;
    this._user.firstName= this._userForm.value['userFirstname'];
    this._user.lastName= this._userForm.value['userLastname'];
    this._user.email= this._userForm.value['userEmail'];
    this._user.birthday= this._userForm.value['userBirthday'];
    this._user.registeredAt = this._userForm.value['userRegisteredAt'];
    this._user.teamId = this._userForm.value['userTeamId'];
    this._userService.createUser(this._user);
    this._router.navigate(['/user']);
  }
}
