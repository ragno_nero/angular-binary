import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/interfaces/component-can-deactivate';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  private _id: number;
  _userForm: FormGroup;
  private _user: User;
  private _saved = false;
  canDeactivate(component: ComponentCanDeactivate): boolean | Observable<boolean> | Promise<boolean> {
    if(!this._saved){
      return confirm("Do you want to leave the page?");
    }
    else{
      return true;
    }
  }
  
  constructor(private _userService: UserService, private _router: Router, private _activatedRoute: ActivatedRoute) { 
    this._userForm = new FormGroup({       
      "userFirstname": new FormControl("", [Validators.required, Validators.minLength(4)]),
      "userLastname": new FormControl("", [Validators.required, Validators.minLength(4)]),
      "userEmail": new FormControl("", [Validators.required, Validators.email]),
      "userBirthday": new FormControl("", Validators.required),
      "userRegisteredAt": new FormControl("", Validators.required),
      "userTeamId": new FormControl("", Validators.required)
  });
  }

  ngOnInit() {
    this._id = this._activatedRoute.snapshot.params.id;
    this._userService.getUser(this._id).subscribe(user=> {
        this._user = user;
        this._userForm.controls['userFirstname'].setValue(this._user.firstName);
        this._userForm.controls['userLastname'].setValue(this._user.lastName);
        this._userForm.controls['userEmail'].setValue(this._user.email);
        this._userForm.controls['userBirthday'].setValue(this._user.birthday);
        this._userForm.controls['userRegisteredAt'].setValue(this._user.registeredAt);
        this._userForm.controls['userTeamId'].setValue(this._user.teamId);
      });   
  }

  save() {
    this._saved = true;
    this._user.firstName= this._userForm.value['userFirstname'];
    this._user.lastName= this._userForm.value['userLastname'];
    this._user.email= this._userForm.value['userEmail'];
    this._user.birthday= this._userForm.value['userBirthday'];
    this._user.registeredAt = this._userForm.value['userRegisteredAt'];
    this._user.teamId = this._userForm.value['userTeamId'];
    this._userService.updateUser(this._id, this._user);
    this._router.navigate(['/user']);
  }
}
