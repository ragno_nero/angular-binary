import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user/user';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss'],
  providers: [UserService]
})
export class ViewUserComponent implements OnInit {
  private userArr: User[];
  constructor(private _userService: UserService) { }

  ngOnInit() {
    this._userService.getUsers().subscribe(user=> 
      {
        this.userArr = user;
      });
  }
}
