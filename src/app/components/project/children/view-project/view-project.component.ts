import { Component, OnInit, Input } from '@angular/core';
import { Project } from 'src/app/models/project/project';
import { ProjectService } from 'src/app/services/project/project.service';

@Component({
  selector: 'app-view-project',
  templateUrl: './view-project.component.html',
  styleUrls: ['./view-project.component.scss'],
  providers: [ProjectService]
})

export class ViewProjectComponent implements OnInit {
  private projArr: Project[];
  constructor(private _projectService: ProjectService) { }

  ngOnInit() {
    this._projectService.getProjects().subscribe(proj=> 
      {
        this.projArr = proj;
      });
  }
}
