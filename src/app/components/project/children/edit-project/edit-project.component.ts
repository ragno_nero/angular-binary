import { Component, OnInit} from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/interfaces/component-can-deactivate';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectService } from 'src/app/services/project/project.service';
import { Project } from 'src/app/models/project/project';
import { FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.scss'],
  providers: [ProjectService]
})

export class EditProjectComponent implements OnInit {
  private _id: number;
  _projectForm: FormGroup;
  private _project: Project;
  private _saved = false;
  canDeactivate(component: ComponentCanDeactivate): boolean | Observable<boolean> | Promise<boolean> {
    if(!this._saved){
      return confirm("Do you want to leave the page?");
    }
    else{
      return true;
    }
  }
  
  constructor(private _projectService: ProjectService, private _router: Router, private _activatedRoute: ActivatedRoute) { 
    this._projectForm = new FormGroup({       
      "projectName": new FormControl("", [Validators.required, Validators.minLength(4)]),
      "projectDescription": new FormControl("", Validators.required),
      "projectCreatedAt": new FormControl("", Validators.required),
      "projectDeadline": new FormControl("", Validators.required),
      "projectAuthorId": new FormControl("", Validators.required),
      "projectTeamId": new FormControl("", Validators.required)
  });
  }

  ngOnInit() {
    this._id = this._activatedRoute.snapshot.params.id;
    this._projectService.getProject(this._id).subscribe(proj=> {
        this._project = proj;
        this._projectForm.controls['projectName'].setValue(this._project.name);
        this._projectForm.controls['projectDescription'].setValue(this._project.description);
        this._projectForm.controls['projectCreatedAt'].setValue(this._project.createdAt);
        this._projectForm.controls['projectDeadline'].setValue(this._project.deadline);
        this._projectForm.controls['projectAuthorId'].setValue(this._project.authorId);
        this._projectForm.controls['projectTeamId'].setValue(this._project.teamId);
      });   
  }

  save() {
    this._saved = true;
    this._project.name= this._projectForm.value['projectName'];
    this._project.description= this._projectForm.value['projectDescription'];
    this._project.createdAt= this._projectForm.value['projectCreatedAt'];
    this._project.deadline= this._projectForm.value['projectDeadline'];
    this._project.authorId= this._projectForm.value['projectAuthorId'];
    this._project.teamId= this._projectForm.value['projectTeamId'];
    this._projectService.updateProjects(this._id, this._project);
    this._router.navigate(['/project']);
  }

}
