import { Component, OnInit} from '@angular/core';
import { UnsavedChangesGuard } from 'src/app/guard/unsaved-changes.guard';
import { ComponentCanDeactivate } from 'src/app/interfaces/component-can-deactivate';
import { Observable } from 'rxjs';
import { ProjectService } from 'src/app/services/project/project.service';
import { Project } from 'src/app/models/project/project';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss'],
  providers: [ProjectService]
})

export class CreateProjectComponent implements OnInit, UnsavedChangesGuard {
  private _project: Project = new Project();
  _projectForm: FormGroup;
  private saved = false;
  canDeactivate(component: ComponentCanDeactivate): boolean | Observable<boolean> | Promise<boolean> {
    if(!this.saved){
      return confirm("Do you want to leave the page?");
    }
    else{
      return true;
    }
  }

  constructor(private _projectService: ProjectService, private _router: Router,) { 
    this._projectForm = new FormGroup({       
      "projectName": new FormControl("", [Validators.required, Validators.minLength(4)]),
      "projectDescription": new FormControl("", Validators.required),
      "projectCreatedAt": new FormControl("", Validators.required),
      "projectDeadline": new FormControl("", Validators.required),
      "projectAuthorId": new FormControl("", Validators.required),
      "projectTeamId": new FormControl("", Validators.required)
  });
  }

  ngOnInit() {
  }

  save(){
    this.saved = true;
    this._project.name= this._projectForm.value['projectName'];
    this._project.description= this._projectForm.value['projectDescription'];
    this._project.createdAt= this._projectForm.value['projectCreatedAt'];
    this._project.deadline= this._projectForm.value['projectDeadline'];
    this._project.authorId= this._projectForm.value['projectAuthorId'];
    this._project.teamId= this._projectForm.value['projectTeamId'];
    this._projectService.createProjects(this._project).subscribe(
      (data: Project) => {console.log("good")},
      error => console.log(error)
  );;
    this._router.navigate(['/project']);
  }
}
