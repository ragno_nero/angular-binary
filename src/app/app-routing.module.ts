import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HomeComponent } from './components/home/home.component';
import { ProjectComponent } from './components/project/project.component';
import { TaskComponent } from './components/task/task.component';
import { TeamComponent } from './components/team/team.component';
import { UserComponent } from './components/user/user.component';
import { UnsavedChangesGuard } from './guard/unsaved-changes.guard';
import { ViewProjectComponent } from './components/project/children/view-project/view-project.component';
import { CreateProjectComponent } from './components/project/children/create-project/create-project.component';
import { ViewTaskComponent } from './components/task/children/view-task/view-task.component';
import { CreateTaskComponent } from './components/task/children/create-task/create-task.component';
import { ViewTeamComponent } from './components/team/children/view-team/view-team.component';
import { CreateTeamComponent } from './components/team/children/create-team/create-team.component';
import { ViewUserComponent } from './components/user/children/view-user/view-user.component';
import { CreateUserComponent } from './components/user/children/create-user/create-user.component';
import { EditProjectComponent } from './components/project/children/edit-project/edit-project.component';
import { EditTaskComponent } from './components/task/children/edit-task/edit-task.component';
import { EditTeamComponent } from './components/team/children/edit-team/edit-team.component';
import { EditUserComponent } from './components/user/children/edit-user/edit-user.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'project', component: ProjectComponent, children: 
    [
      { path: '', component: ViewProjectComponent },
      { path: 'edit/:id', component: EditProjectComponent, canDeactivate: [UnsavedChangesGuard] },
      { path: 'create', component: CreateProjectComponent, canDeactivate: [UnsavedChangesGuard] },
    ] 
  },
  { path: 'task', component: TaskComponent, children: 
    [
      { path: '', component: ViewTaskComponent },
      { path: 'edit/:id', component: EditTaskComponent, canDeactivate: [UnsavedChangesGuard] },
      { path: 'create', component: CreateTaskComponent, canDeactivate: [UnsavedChangesGuard] },
    ] 
  },
  { path: 'team', component: TeamComponent, children: 
    [
      { path: '', component: ViewTeamComponent },
      { path: 'edit/:id', component: EditTeamComponent, canDeactivate: [UnsavedChangesGuard] },
      { path: 'create', component: CreateTeamComponent, canDeactivate: [UnsavedChangesGuard] },
    ] 
  },
  { path: 'user', component: UserComponent, children: 
    [
      { path: '', component: ViewUserComponent },
      { path: 'edit/:id', component: EditUserComponent, canDeactivate: [UnsavedChangesGuard] },
      { path: 'create', component: CreateUserComponent, canDeactivate: [UnsavedChangesGuard] },
    ] 
  },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
